function findMissingNumber(arr) {
    arr.sort((a, b) => a - b); //Отсортировали массив по возрастанию
    let missingNumber = null;
  
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] !== i + arr[0]) {
        //Пробегаемся по отсортированному массиву и сравниваем значение с индексом + первый элемент (не все массивы могут начинаться с нуля)
        missingNumber = i + arr[0]; //Если значения не совпадают, это и есть недостающее значение
        break;
      }
    }
  
    if (missingNumber === null) {
      // Если недостающих значений не найдено - значит не хватает последнего
      missingNumber = arr.length + arr[0];
    }
  
    return missingNumber;
  }
  
  // Тесты
  console.log(findMissingNumber([5, 0, 1, 3, 2])); // 4
  console.log(findMissingNumber([7, 9, 10, 11, 12])); // 8
  console.log(findMissingNumber([7, 10, 11, 12, 8, 9])); // 13
  console.log(findMissingNumber([0, -2, 1, 2]));// -1
  
  //Т.к. неясно что делать при отсутствии недостающих значений в середине списка, решил взять за недостающее последнее значение и возвращать его, можно сделать так же с первым.
  